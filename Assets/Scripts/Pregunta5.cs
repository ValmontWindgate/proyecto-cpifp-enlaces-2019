﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pregunta5 : MonoBehaviour {
	public Rigidbody rigidbody;
	
	// Update is called once per frame
	void Update () {
		rigidbody.AddForce(Vector3.right *Input.GetAxis("Horizontal")*1f );
		rigidbody.AddForce(Vector3.forward *Input.GetAxis("Vertical")*1f );
	}
}
