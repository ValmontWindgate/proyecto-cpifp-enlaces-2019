﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pregunta1 : MonoBehaviour {

	public int balas;
	public float peso;
	public string nombre;
	public bool interruptor;

	public Vector2 direccionViento;	
	public Color sangre;
	public Color[] bandera;
	public List<Color> banderaGuay; 
	public Color32 colorRaro;

	public Space espacio;
	public ForceMode modoFuerza;
	public LayerMask mascara;

	public enum TpPuntoCardinal {Norte=1 , Este , Sur , Oeste};
	public TpPuntoCardinal direccionPersonaje; 
	public List<TpPuntoCardinal> solucionLaberinto;

	public Texture agujeroBala;
	public AudioClip disparo;
	public Mesh bala;

	public Text UIVelocidad;
	public Button explotarCiudad;

 
	public Rigidbody rigidbodyEnemigo; 	
	public LODGroup lodGroup; 

	public GameObject cajaMadera;

	void Start () {
		
	}
	
	void Update () {
		
	}
}
