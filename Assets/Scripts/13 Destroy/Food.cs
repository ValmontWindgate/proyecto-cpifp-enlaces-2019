﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {

	public Rigidbody rigidbody;

	void Start ( ) {
		Destroy ( gameObject , 3 );		// Programamos la autodestrucción del objeto entero a los 3 segundos de vida
		Destroy ( rigidbody , 1 );		// Programamos destrucción del Rigidbody pasados 1 segundos
		Destroy ( this );				// Autodestruimos el script para que no consuma, ya ha terminado su tarea 
	}
	
}
