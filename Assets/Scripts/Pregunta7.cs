﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pregunta7 : MonoBehaviour {
	public Rigidbody rigidbody;
	public float velocidad = 1f;
	
	// Update is called once per frame
	void Update () {
		/* 
		transform.Translate(Vector3.forward * 1f *Input.GetAxis("Vertical") * Time.deltaTime);
		if (Input.GetKey(KeyCode.Space))
		{
			transform.Translate(Vector3.forward * 0.5f * Time.deltaTime);
		}
		*/
		rigidbody.velocity = Vector3.forward * velocidad;
		if (Input.GetKeyDown(KeyCode.Space))
		{
			velocidad += 0.5f * Time.deltaTime;
		}
	}
}
