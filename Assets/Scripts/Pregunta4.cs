﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pregunta4 : MonoBehaviour {
	public GameObject cubito;
	
	// Update is called once per frame
	void Update () {
		transform.position += Vector3.forward * Input.GetAxis("Mouse Y");
		transform.position += Vector3.right * Input.GetAxis("Mouse X");
	}
}
