﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLerp : MonoBehaviour {
	public GameObject target;
	// Update is called once per frame
	void Update () {
		transform.position=Vector3.Lerp(transform.position , target.transform.position, 0.05f);
		transform.rotation=Quaternion.Lerp(transform.rotation, target.transform.rotation, 0.05f);
		target.transform.Translate(Vector3.forward * Input.GetAxis ("Mouse ScrollWheel") * 100f * Time.deltaTime);
	}
}
