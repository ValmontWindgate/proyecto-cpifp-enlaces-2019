﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	// 1. Conseguir que se clone dentro de un área esférica de 5 metros alrededor del spawn (Random)
	// 2. Conseguir que el área sea CIRCULAR de 5 metros alrededor del spawner (Vector3)
	// 3. Conseguir que el spawner se destruya cuando ha generado 100 clones (Destroy)
	// 4. Conseguir que el spawner clone un objeto por segundo (InvokeRepeating)
	// 5. Conseguir que el objeto a spawnear sea uno aleatorio de una lista

	public GameObject prefab;	// Aquí arrastraremos el prefab a clonar
	public GameObject clone;	// Con esta variable obtendremos acceso al clon
	public Vector3 posicion;	// Variable auxiliar para poder calcular una posición aleatoria de clonado
	
	void Start ( ) {
		InvokeRepeating ( "Shoot" , 1f , 1f );	// Invocamos el método Shoot cada segundo
		Destroy ( gameObject , 10 );			// El spawner se autodestruye transcurridos 10 segundos
	}

	// Método que se ocupa de clonar, se ejecuta sólamente si nosotos lo invocamos
	void Shoot () {
		// Generamos un nuevo vector de posición para aleatorizar la posición de clonado
		posicion =  new Vector3 ( Random.Range(-5.0f, 5.0f), 0, Random.Range(-5.0f, 5.0f) );
		clone = Instantiate ( prefab , transform.position+posicion, transform.rotation );
	}
	
}
