using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escena8 : MonoBehaviour {

	public Color color1;
	public Color color2;
	public Color lerpColor;
	
	
	// Update is called once per frame
	void Update () {
		
		
		//color1 = new Color ( Random.Range ( 0.0f , 1.0f ) , Random.Range ( 0.0f , 1.0f ) , Random.Range ( 0.0f , 1.0f ) );
		//color2 = new Color ( Random.Range ( 0.0f , 1.0f ) , Random.Range ( 0.0f , 1.0f ) , Random.Range ( 0.0f , 1.0f ) );

		lerpColor = Color.Lerp (color1,color2, Mathf.PingPong(Time.time, 1) );
	}
}
