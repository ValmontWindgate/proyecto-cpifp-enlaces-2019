﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pregunta9 : MonoBehaviour {
	public Animator animator;

	// Update is called once per frame
	void Update () {
		animator.SetFloat ("Eje Horizontal" , Input.GetAxis ("Horizontal"));
		animator.SetFloat ("Eje Vertical" , Input.GetAxis ("Vertical"));
	}
}
