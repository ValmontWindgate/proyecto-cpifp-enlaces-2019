﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pregunta8 : MonoBehaviour {
	public GameObject esfera;
	public GameObject target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.R))
		{
			target.transform.position = Vector3.forward * Random.Range (15f , -15f);
			target.transform.position += Vector3.right * Random.Range (15f , -15f);
			target.transform.position += Vector3.up * Random.Range (15f , -15f);
		}
	}
}
